package fgenaudet.ch.aarlibtest;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.PointF;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.artifex.mupdflib.AsyncTask;
import com.artifex.mupdflib.MuPDFCore;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;


public class MainActivity extends ActionBarActivity {
    private static final String TEST_FILE_NAME = "pdf_form.pdf";
    private MuPDFCore core;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        File fo = getFilesDirectory(getApplicationContext());
        final File fi = new File(fo, TEST_FILE_NAME);


        if (!fi.exists()) {
            copyTestDocToSdCard(fi);
        }

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                try {
                    core = new MuPDFCore(MainActivity.this, fi.getAbsolutePath());
                    core.countPages();
                    core.setDisplayPages(1);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... params) {
                int pageNumber = 1;
                try {
                    final ImageView preview = (ImageView) findViewById(R.id.preview);
                    PointF pageSize = core.getPageSize(pageNumber);
                    final Bitmap bmp = Bitmap.createBitmap((int) pageSize.x, (int) pageSize.y, Bitmap.Config.ARGB_8888);
                    core.drawSinglePage(pageNumber, bmp, (int) pageSize.x, (int) pageSize.y);

                    final Bitmap scaledBitmap = Bitmap.createScaledBitmap(bmp, 200, 300, false);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            preview.setImageBitmap(scaledBitmap);
                        }
                    });

                }  catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }


    private void copyTestDocToSdCard(final File testImageOnSdCard) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    InputStream is = getAssets().open(TEST_FILE_NAME);
                    FileOutputStream fos = new FileOutputStream(testImageOnSdCard);
                    byte[] buffer = new byte[8192];
                    int read;
                    try {
                        while ((read = is.read(buffer)) != -1) {
                            fos.write(buffer, 0, read);
                        }
                    } finally {
                        fos.flush();
                        fos.close();
                        is.close();
                    }
                } catch (IOException e) {

                }
            }
        }).start();
    }

    public static File getFilesDirectory(Context context) {
        File appFilesDir = null;
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            appFilesDir = getExternalFilesDir(context);
        }
        if (appFilesDir == null) {
            appFilesDir = context.getFilesDir();
        }
        return appFilesDir;
    }

    private static File getExternalFilesDir(Context context) {
        File dataDir = new File(new File(Environment.getExternalStorageDirectory(), "Android"), "data");
        File appFilesDir = new File(new File(dataDir, context.getPackageName()), "files");
        if (!appFilesDir.exists()) {
            if (!appFilesDir.mkdirs()) {
                //L.w("Unable to create external cache directory");
                return null;
            }
            try {
                new File(appFilesDir, ".nomedia").createNewFile();
            } catch (IOException e) {
                //L.i("Can't create \".nomedia\" file in application external cache directory");
                return null;
            }
        }
        return appFilesDir;
    }
}
